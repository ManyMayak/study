package com.example.erik.mymovies.utils;

import com.example.erik.mymovies.pojo.ProductionCompanies;

import java.util.List;

public class OtherUtils {

    public static String getCode(List<ProductionCompanies> productionCompanies){
        String code = "";
        int i = 0;

        while (i<productionCompanies.size() && code.isEmpty()){
            code = productionCompanies.get(i).
                    getOriginCountry();
            i++;
        }
        return code;
    }





}
