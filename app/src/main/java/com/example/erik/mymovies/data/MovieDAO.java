package com.example.erik.mymovies.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.erik.mymovies.pojo.Movie;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface MovieDAO {

    @Query("SELECT * FROM movies")
    LiveData<List<Movie>> getAllMovies();

    @Query("SELECT * FROM favourite_movies")
    LiveData<List<FavouriteMovies>> getAllFavouriteMovies();

    @Query("SELECT * FROM movies WHERE id == :movieId")
    Single<Movie> getMovieById(int movieId);

    @Query("DELETE FROM movies")
    void deleteAllMovies();


    @Insert
    void insertMovie(List<Movie> movies);

    @Delete
    void deleteMovie(List<Movie> movies);

    @Insert
    void insertFavoriteMovie(FavouriteMovies movie);

    @Delete
    void deleteFavoriteMovie(FavouriteMovies movie);

    @Query("SELECT * FROM favourite_movies WHERE id == :movieId")
    Single<FavouriteMovies> getFavouriteMovieById(int movieId);
}
