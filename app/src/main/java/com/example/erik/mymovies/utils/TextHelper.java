package com.example.erik.mymovies.utils;

import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.annotation.NonNull;


public class TextHelper {

    public static void makeTextViewResizable(final TextView tv, final int maxLine,
                                             final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().
                            getLineEnd(0);
                    String text = tv.getText().
                            subSequence(0, lineEndIndex - expandText.length() + 1) + " " +
                            expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(
                                    tv.getText().toString()), tv, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().
                            getLineEnd(maxLine - 1);
                    String text = tv.getText().
                            subSequence(0, lineEndIndex - expandText.length() + 1) + " " +
                            expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(
                                    tv.getText().toString()), tv, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().
                            getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().
                            subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(Html.fromHtml(
                                    tv.getText().toString()), tv, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    private static SpannableStringBuilder addClickablePartTextViewResizable(final Spanned strSpanned,
                                                                            final TextView tv,
                                                                            final String spanableText,
                                                                            final boolean viewMore) {
        String str = strSpanned.toString();
        int index;
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);
        if (viewMore){
            index = 3;
        } else {
            index = 0;
        }
        if (str.contains(spanableText)) {


            ssb.setSpan(new SpannableMore(false){
                @Override
                public void onClick(@NonNull View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, -1, "See Less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(tv, 4, "... show more.", true);
                    }
                }
            }, str.indexOf(spanableText)+index, str.indexOf(spanableText) +
                    spanableText.length(), 0);

        }
        return ssb;

    }
}
