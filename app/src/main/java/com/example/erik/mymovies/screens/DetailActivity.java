package com.example.erik.mymovies.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.erik.mymovies.R;
import com.example.erik.mymovies.adapters.ReviewAdapter;
import com.example.erik.mymovies.adapters.TrailerAdapter;
import com.example.erik.mymovies.data.FavouriteMovies;
import com.example.erik.mymovies.pojo.GenreResponse;
import com.example.erik.mymovies.utils.OtherUtils;
import com.example.erik.mymovies.pojo.Movie;
import com.example.erik.mymovies.utils.ImageHelper;
import com.example.erik.mymovies.utils.TextHelper;
import com.squareup.picasso.Picasso;


import java.util.Locale;
import java.util.Objects;

public class DetailActivity extends AppCompatActivity {

    private ImageView imageViewAddToFavourite, imageViewBigPoster, imageViewFlag;
    private TextView textViewLength, textViewLanguage, textViewCountry, textViewTitle, textViewVote,
            textViewYear, textViewDescription;
    private RatingBar ratingBarMovie;

    private View.OnClickListener onClickListener;

    private int id;

    private MainViewModel mainViewModel;

    private Movie movies;
    private FavouriteMovies favouriteMoviess;

    private TrailerAdapter trailerAdapter;
    private ReviewAdapter reviewAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.item_main:
                Intent intentMain = new Intent(this, MainActivity.class);
                startActivity(intentMain);
                break;
            case R.id.item_favourite:
                Intent intentFavourite = new Intent(this, FavouriteActivity.class);
                startActivity(intentFavourite);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        imageViewBigPoster = findViewById(R.id.imageViewBigPoster);
        imageViewAddToFavourite = findViewById(R.id.imageViewAddFavourite);
        ImageView imageViewBack = findViewById(R.id.imageViewBack);
        imageViewFlag = findViewById(R.id.imageViewFlag);
        ImageView imageViewPlayTrailer = findViewById(R.id.imageViewPlayTrailer);
        ImageView imageViewShare = findViewById(R.id.imageViewShare);
        textViewTitle = findViewById(R.id.textViewTitleValue);
        textViewVote = findViewById(R.id.textViewRatingValue);
        textViewDescription = findViewById(R.id.textViewDescriptionValue);
        textViewLanguage = findViewById(R.id.textViewLanguageValue);
        textViewLength = findViewById(R.id.textViewLengthValue);
        textViewCountry =findViewById(R.id.textViewCountryValue);
        textViewYear = findViewById(R.id.textViewYearValue);
        ScrollView scrVInfo = findViewById(R.id.scrollViewInfo);
        ratingBarMovie = findViewById(R.id.ratingBarFilm);

        dataRetrieval();

        setOnFavourite();

        createListener();

        imageViewAddToFavourite.setOnClickListener(onClickListener);
        imageViewBack.setOnClickListener(onClickListener);
        imageViewPlayTrailer.setOnClickListener(onClickListener);
        imageViewShare.setOnClickListener(onClickListener);

        RecyclerView recyclerViewTrailers = findViewById(R.id.recyclerViewTrailers);
        RecyclerView recyclerViewReview = findViewById(R.id.recyclerViewReviews);

        createTrailerAdapter();
        createReviewAdapter();

        recyclerViewTrailers.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewReview.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTrailers.setAdapter(trailerAdapter);
        recyclerViewReview.setAdapter(reviewAdapter);



        scrVInfo.smoothScrollTo(0,0);
    }

    private void dataRetrieval(){
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("id")){
            id = intent.getIntExtra("id", -1);
        }else {
            finish();
        }

        customizationMainViewModel(id);
    }

    @SuppressLint("SetTextI18n")
    private void customizationMainViewModel(int id){
        String lang = Locale.getDefault().
                getLanguage();

        mainViewModel = ViewModelProvider.AndroidViewModelFactory.
                getInstance(getApplication()).
                create(MainViewModel.class);

        mainViewModel.getMovies(id);


        mainViewModel.getMovieLiveData().
                observe(this, movie -> {

            movies = movie;

            Picasso.get().
                    load(movies.getBigPosterPath()).
                    placeholder(R.drawable.placeholder).
                    transform(new ImageHelper(334, 0)).
                    into(imageViewBigPoster);

            textViewTitle.setText(movies.getTitle());
            textViewVote.setText(movies.converterToPercent()+"%");
            textViewYear.setText(movies.getYear());
            ratingBarMovie.setRating((float)movies.getVoteAverage()/2);
            textViewDescription.setText(movies.getOverview());
            mainViewModel.loadTrailerMovie(movies.getId(), lang);
            mainViewModel.loadReviewsMovie(movies.getId(), lang);
            mainViewModel.loadInfoMovie(movies.getId(), lang);
            TextHelper.makeTextViewResizable(textViewDescription, 3,
                    "... show more", true);

        });

        mainViewModel.getFavouriteMoviesLiveData().
                observe(this, favouriteMovies -> {
                    favouriteMoviess = favouriteMovies;
                    setOnFavourite();

        });

        mainViewModel.getGenreResponseLiveData().
                observe(this, genreResponse -> {

            if (genreResponse.getOriginalLanguage() == null) {
                textViewLanguage.setText("-");
            } else {
                textViewLanguage.setText(genreResponse.getOriginalLanguage().toUpperCase());
            }
            textViewLength.setText(genreResponse.getRuntime()+"m");
            textViewCountry.setText(OtherUtils.getCode(genreResponse.getProductionCompanies()));
            Picasso.get().
                    load(genreResponse.getFlagUrl()).
                    into(imageViewFlag);
            addGenre(genreResponse);
        });
    }

    private void createListener(){
        onClickListener = view -> {
            switch (view.getId()) {
                case R.id.imageViewAddFavourite:
                    addFavourite();
                    break;
                case R.id.imageViewBack:
                    onBackPressed();
                    break;
                case R.id.imageViewPlayTrailer:
                    transitionExternalSource(Objects.requireNonNull(mainViewModel.getTrailers().
                            getValue()).get(0).getPathTrailer());
                    break;
                case R.id.imageViewShare:
                    share(movies.pathSiteMovie());
                    break;
            }
        };
    }

    private void createTrailerAdapter(){
        trailerAdapter = new TrailerAdapter();
        mainViewModel.getTrailers().
                observe(this, trailers -> trailerAdapter.setTrailers(trailers));

        trailerAdapter.setTrailerOnClickListener(this::transitionExternalSource);

    }

    private void createReviewAdapter(){
        reviewAdapter = new ReviewAdapter();
        mainViewModel.getReviews().
                observe(this, reviews -> reviewAdapter.setReviews(reviews));
    }

    private void addFavourite(){

        if (favouriteMoviess == null){
            mainViewModel.insertFavouriteMovie(new FavouriteMovies(movies));
            Toast.makeText(DetailActivity.this, getString(R.string.add_to_favourite), Toast.
                    LENGTH_SHORT).
                    show();
            imageViewAddToFavourite.setImageResource(R.drawable.staron);
        } else {
            mainViewModel.deleteFavouriteMovie(favouriteMoviess);
            Toast.makeText(DetailActivity.this, getString(R.string.delete_from_favourite), Toast.
                    LENGTH_SHORT).
                    show();
            imageViewAddToFavourite.setImageResource(R.drawable.staroff);
        }

        mainViewModel.getFavouriteMovie(id);
    }

    private void transitionExternalSource(String url){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    private void share(String url){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }

    private void setOnFavourite(){
        if(favouriteMoviess == null){
            imageViewAddToFavourite.setImageResource(R.drawable.staroff);
        }else{
            imageViewAddToFavourite.setImageResource(R.drawable.staron);
        }
    }

    private void addGenre(GenreResponse genreResponse){
        LinearLayout constraintLayout = findViewById(R.id.LinearLayoutGenres);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.
                LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.
                LayoutParams.WRAP_CONTENT);
        layoutParams.rightMargin = 10;
        GradientDrawable drawable;
        for (int i=0; i<genreResponse.getGenres().size(); i++){
            TextView genre = new TextView(this);
            genre.setText(genreResponse.getGenres().get(i).getName().toUpperCase());
            genre.setTextSize(13);
            genre.setTextColor(colorDefinition(genre.getText().toString()));
            genre.setPadding(17,6,17,6);
            genre.setBackground(getResources().getDrawable(R.drawable.border_textview));
            drawable = (GradientDrawable)genre.getBackground();
            drawable.setStroke(2, colorDefinition(genre.getText().toString()));
            constraintLayout.addView(genre, layoutParams);
        }
        constraintLayout.setGravity(Gravity.CENTER);
    }

    private int colorDefinition(String genre){
        switch (genre){
            case "ACTION":
                return getResources().getColor(R.color.action);
            case "SCIENCE FICTION":
                return getResources().getColor(R.color.sci_fi);
            case "ADVENTURE":
                return getResources().getColor(R.color.adventure);
            case "COMEDY":
                return getResources().getColor(R.color.comedy);
            case "DOCUMENTARY":
                return getResources().getColor(R.color.biography);
            case "DRAMA":
                return getResources().getColor(R.color.drama);
            case "CRIME":
                return getResources().getColor(R.color.crime);
            default:
                return getResources().getColor(R.color.borderTextView);
        }
    }


}
