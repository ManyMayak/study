package com.example.erik.mymovies.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.erik.mymovies.R;
import com.example.erik.mymovies.adapters.MovieAdapter;
import com.example.erik.mymovies.data.FavouriteMovies;
import com.example.erik.mymovies.pojo.Movie;

import java.util.ArrayList;
import java.util.List;

public class FavouriteActivity extends AppCompatActivity {

    private MovieAdapter movieAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.item_main:
                Intent intentMain = new Intent(this, MainActivity.class);
                startActivity(intentMain);
                break;
            case R.id.item_favourite:
                Intent intentFavourite = new Intent(this, FavouriteActivity.class);
                startActivity(intentFavourite);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);

        RecyclerView recyclerViewFavourite = findViewById(R.id.recyclerViewFavourite);
        recyclerViewFavourite.setLayoutManager(new GridLayoutManager(this,2));

        createMovieAdapter();

        recyclerViewFavourite.setAdapter(movieAdapter);


        createMainViewModel();

    }

    private void createMovieAdapter(){
        movieAdapter = new MovieAdapter();

        movieAdapter.setOnClickRecyclerItem(position -> {
            Movie movie = movieAdapter.getArrayListMovie().
                    get(position);
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra("id", movie.getId());
            startActivity(intent);
        });
    }

    private void createMainViewModel(){
        MainViewModel mainViewModel = ViewModelProvider.AndroidViewModelFactory.
                getInstance(getApplication()).
                create(MainViewModel.class);
        LiveData<List<FavouriteMovies>> favouriteMovies = mainViewModel.getFavouriteMovies();
        favouriteMovies.observe(this, favouriteMovies1 -> {
            List<Movie> movies = new ArrayList<>(favouriteMovies1);
            movieAdapter.setArrayListMovie(movies);
        });
    }
}
