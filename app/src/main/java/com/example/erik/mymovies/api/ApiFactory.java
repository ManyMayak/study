package com.example.erik.mymovies.api;

import android.content.Context;

import com.example.erik.mymovies.R;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiFactory {

    private static ApiFactory apiFactory;
    private static Retrofit retrofit;

    private ApiFactory(Context context){
        retrofit = new Retrofit.Builder().
                addConverterFactory(GsonConverterFactory.create()).
                addCallAdapterFactory(RxJava2CallAdapterFactory.create()).
                baseUrl(context.getString(R.string.base_url)).
                build();
    }

    public static ApiFactory getInstance(Context context){
        if (apiFactory == null) {
            apiFactory = new ApiFactory(context);
        }
        return apiFactory;
    }

    public ApiService getApiService(){
        return retrofit.create(ApiService.class);
    }

}
