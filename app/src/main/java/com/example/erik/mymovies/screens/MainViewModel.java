package com.example.erik.mymovies.screens;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Transaction;

import com.example.erik.mymovies.R;
import com.example.erik.mymovies.api.ApiFactory;
import com.example.erik.mymovies.api.ApiService;
import com.example.erik.mymovies.data.FavouriteMovies;
import com.example.erik.mymovies.data.MovieDatabase;
import com.example.erik.mymovies.pojo.GenreResponse;
import com.example.erik.mymovies.pojo.Movie;
import com.example.erik.mymovies.pojo.Review;
import com.example.erik.mymovies.pojo.Trailer;
import com.example.erik.mymovies.utils.NetworkUtils;
import com.example.erik.mymovies.utils.OtherUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends AndroidViewModel {

    private static MovieDatabase movieDatabase;

    private LiveData<List<Movie>> movies;
    private LiveData<List<FavouriteMovies>> favouriteMovies;
    private MutableLiveData<List<Trailer>> trailers;
    private MutableLiveData<List<Review>> reviews;
    private MutableLiveData<GenreResponse> genreResponseLiveData;
    private CompositeDisposable compositeDisposable;
    private ApiService apiService;
    private Disposable disposable;
    private MutableLiveData<Movie> movieLiveData;
    private MutableLiveData<FavouriteMovies> favouriteMoviesLiveData;

    private static final String SORT_BY_POPULARITY = "popularity.desc";
    private static final String SORT_BY_AVERAGE_VOTE = "vote_average.desc";
    private static final String VALUE_VOTE_COUNT = "1000";

    private static int page = 1;

    private static final int POPULARITY = 0;

    private boolean isLoading = true;

    private String API_KEY;

    public MainViewModel(@NonNull Application application) {
        super(application);
        API_KEY = application.getString(R.string.api_key);
        movieDatabase = MovieDatabase.getInstance(getApplication());
        movies = movieDatabase.getMovieDAO().
                getAllMovies();
        favouriteMovies = movieDatabase.getMovieDAO().
                getAllFavouriteMovies();
        trailers = new MutableLiveData<>();
        reviews = new MutableLiveData<>();
        genreResponseLiveData = new MutableLiveData<>();
        movieLiveData = new MutableLiveData<>();
        favouriteMoviesLiveData = new MutableLiveData<>();

        ApiFactory apiFactory = ApiFactory.getInstance(application);
        apiService = apiFactory.getApiService();
        compositeDisposable = new CompositeDisposable();
    }

    public LiveData<List<Movie>> getMovies() {
        return movies;
    }

    LiveData<List<FavouriteMovies>> getFavouriteMovies() {
        return favouriteMovies;
    }

    LiveData<List<Trailer>> getTrailers() {
        return trailers;
    }

    LiveData<List<Review>> getReviews() {
        return reviews;
    }

    LiveData<GenreResponse> getGenreResponseLiveData() {
        return genreResponseLiveData;
    }

    LiveData<Movie> getMovieLiveData() {
        return movieLiveData;
    }

    private void setMovieLiveData(MutableLiveData<Movie> movieLiveData) {
        this.movieLiveData = movieLiveData;
    }

    MutableLiveData<FavouriteMovies> getFavouriteMoviesLiveData() {
        return favouriteMoviesLiveData;
    }

    public void setFavouriteMoviesLiveData(MutableLiveData<FavouriteMovies> favouriteMoviesLiveData) {
        this.favouriteMoviesLiveData = favouriteMoviesLiveData;
    }

    private void clear(){
        favouriteMoviesLiveData.setValue(null);
    }

    static int getPage() {
        return page;
    }

    static void setPage(int page) {
        MainViewModel.page = page;
    }

    boolean isLoading() {
        return isLoading;
    }

    void setLoading() {
        isLoading = false;
    }

    void getMovies(int id) {
        movieDatabase.getMovieDAO().
                getMovieById(id).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new DisposableSingleObserver<Movie>() {
                    @Override
                    public void onSuccess(Movie movie) {
                        movieLiveData.setValue(movie);
                        getFavouriteMovie(movie.getId());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });

    }

    void loadData(int sortBy, String lang){
        isLoading = true;
        disposable = apiService.getMovies(API_KEY, lang, defineMethodSort(sortBy),
                VALUE_VOTE_COUNT, Integer.toString(page)).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(movieResponse -> {
                    insertAndDelete(movieResponse.getResults());
                    setPage(getPage()+1);
                    deleteMovie(movieResponse.getResults());

                }, throwable -> {

                });
        compositeDisposable.add(disposable);

    }

    @Transaction
    private void insertAndDelete(List<Movie> rMovieList){
        if (page  == 1) {
            deleteAllMovies();
        }
        insertMovie(rMovieList);
    }

    void loadTrailerMovie(int id, String lang){
        disposable = apiService.getTrailers(id, API_KEY, lang).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(trailerResponse -> trailers.setValue(trailerResponse.getResults()),
                        throwable -> {

                });
        compositeDisposable.add(disposable);
    }

    void loadReviewsMovie(int id, String lang){
        disposable = apiService.getReviews(id, API_KEY, lang).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(reviewResponse -> reviews.setValue(reviewResponse.getResults()),
                        throwable -> {
                });
        compositeDisposable.add(disposable);
    }

    void loadInfoMovie(int id, String lang){
        disposable = apiService.
                getGenres(id, API_KEY, lang).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new Consumer<GenreResponse>() {
                    /**
                     * Consume the given value.
                     *
                     * @param genreResponse the value
                     */
                    @Override
                    public void accept(GenreResponse genreResponse) {
                        genreResponse.setFlagUrl(NetworkUtils.buildFlagsUrl(OtherUtils.
                                getCode(genreResponse.getProductionCompanies())));
                        genreResponseLiveData.setValue(genreResponse);

                    }
                });
        compositeDisposable.add(disposable);
    }


    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     * <p>
     * It is useful when ViewModel observes some data and you need to clear this subscription to
     * prevent a leak of this ViewModel.
     */
    @Override
    protected void onCleared() {
        compositeDisposable.dispose();
        super.onCleared();
    }

    private String defineMethodSort(int sortBy){
        String methodSort;
        if (sortBy == POPULARITY){
            methodSort = SORT_BY_POPULARITY;
        }else {
            methodSort = SORT_BY_AVERAGE_VOTE;
        }
        return methodSort;
    }

    private void deleteMovie(List<Movie> movie){
        Observable.fromCallable(() -> {
            if(movie!=null && movie.size()>0) {
                movieDatabase.getMovieDAO().
                        deleteMovie(movie);
            }
            return true;
        })
        .subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe();
    }

    private void insertMovie(List<Movie> movie){

        Observable.fromCallable(() -> {
                movieDatabase.getMovieDAO().
                        insertMovie(movie);
            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).
                subscribe();
    }

    private void deleteAllMovies(){

        Observable.fromCallable(() -> {
                movieDatabase.getMovieDAO().
                        deleteAllMovies();

            return true;
        })
                .subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe();

    }

    void insertFavouriteMovie(FavouriteMovies movie){
        Observable.fromCallable(() -> {
            movieDatabase.getMovieDAO().
                    insertFavoriteMovie(movie);


            return true;
        })
                .subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe();
    }

    void deleteFavouriteMovie(FavouriteMovies movie){
        Observable.fromCallable(() -> {
            movieDatabase.getMovieDAO().
                    deleteFavoriteMovie(movie);
            return true;
        })
                .subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe();
        clear();

    }

    void getFavouriteMovie(int id){
        movieDatabase.getMovieDAO().
                getFavouriteMovieById(id).
                subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(new DisposableSingleObserver<FavouriteMovies>() {
            @Override
            public void onSuccess(FavouriteMovies favouriteMovies) {
                favouriteMoviesLiveData.setValue(favouriteMovies);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
