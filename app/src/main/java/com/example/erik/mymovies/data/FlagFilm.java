package com.example.erik.mymovies.data;

public class FlagFilm {

    private String flag;

    public FlagFilm(String flag) {
        this.flag = flag;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }
}
