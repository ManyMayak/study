package com.example.erik.mymovies.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import com.squareup.picasso.Transformation;


public class ImageHelper implements Transformation {
    private final int radius;
    private final int margin;

    public ImageHelper(final int radius, final int margin) {
        this.radius = radius;
        this.margin = margin;
    }

    @Override
    public Bitmap transform(final Bitmap source) {
        int w = source.getWidth();
        int h = source.getHeight();
        Bitmap output = Bitmap.createBitmap(w, h+20, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        Path path = new Path();

        path.moveTo(0,0);
        path.lineTo(0, h-80);
        path.quadTo(w/2, h, w, h-80);
        path.lineTo(w, 0);
        path.lineTo(0,0);

        final RectF rectF = new RectF(0, 0, w, h);
        final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        canvas.drawPath(path, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(source, null, rectF, paint);

        Path path1 = new Path();
        path1.moveTo(0,h-80);
        path1.quadTo(w/2, h+10, w, h-80);
        path1.quadTo(w/2, h, 0, h-80);

        Paint red = new Paint();

        red.setColor(android.graphics.Color.RED);

        canvas.drawPath(path1, red);


        source.recycle();
        return output;
    }

    @Override
    public String key() {
        return "rounded";
    }


}


