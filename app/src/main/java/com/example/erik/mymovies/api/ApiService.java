package com.example.erik.mymovies.api;


import com.example.erik.mymovies.data.FlagFilm;
import com.example.erik.mymovies.pojo.GenreResponse;
import com.example.erik.mymovies.pojo.MovieResponse;
import com.example.erik.mymovies.pojo.ReviewResponse;
import com.example.erik.mymovies.pojo.TrailerResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiService {

    @GET("discover/movie")
    Observable<MovieResponse> getMovies(@Query("api_key") String apiKey, @Query("language") String
            language, @Query("sort_by") String methodSort, @Query("vote_count.gte") String
            voteCount, @Query("page") String page);

    @GET("movie/{id}/videos")
    Observable<TrailerResponse> getTrailers(@Path("id") int id, @Query("api_key") String apiKey,
            @Query("language") String language);

    @GET("movie/{id}/reviews")
    Observable<ReviewResponse> getReviews(@Path("id") int id, @Query("api_key") String apiKey,
                                           @Query("language") String language);

    @GET("movie/{id}")
    Observable<GenreResponse> getGenres(@Path("id") int id, @Query("api_key") String apiKey,
                                         @Query("language") String language);

}
