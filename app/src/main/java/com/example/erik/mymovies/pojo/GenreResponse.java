package com.example.erik.mymovies.pojo;

import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GenreResponse {

    @SerializedName("genres")
    @Expose
    private List<Genre> genres = null;
    @SerializedName("original_language")
    @Expose
    private String originalLanguage;
    @SerializedName("production_companies")
    @Expose
    private List<ProductionCompanies> productionCompanies = null;
    @SerializedName("runtime")
    @Expose
    private int runtime;

    private Uri flagUrl;

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public List<ProductionCompanies> getProductionCompanies() {
        return productionCompanies;
    }

    public void setProductionCompanies(List<ProductionCompanies> productionCompanies) {
        this.productionCompanies = productionCompanies;
    }

    public Uri getFlagUrl() {
        return flagUrl;
    }

    public void setFlagUrl(Uri flagUrl) {
        this.flagUrl = flagUrl;
    }
}
