package com.example.erik.mymovies.utils;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

import androidx.annotation.NonNull;

public class SpannableMore extends ClickableSpan {

    private boolean isUnderline;

    /**
     * Constructor
     */
    SpannableMore(boolean isUnderline) {
        if (isUnderline) this.isUnderline = true;
        else this.isUnderline = false;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        ds.setUnderlineText(isUnderline);

        ds.setColor(Color.parseColor("#fbc02d"));
    }

    @Override
    public void onClick(@NonNull View view) {

    }
}
