package com.example.erik.mymovies.pojo;

import android.annotation.SuppressLint;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Entity(tableName = "movies")
public class Movie {

    private static final String BASE_URL_POSTER = "https://image.tmdb.org/t/p/";
    private static final String SIZE_SMALL_POSTER = "w185";
    private static final String SIZE_BIG_POSTER = "w780";
    private static final String BASE_URL_MOVIE_SITE = "https://www.themoviedb.org/";

    @PrimaryKey(autoGenerate = true)
    private int uniqId;
    @SerializedName("vote_count")
    @Expose
    private int voteCount;
    @SerializedName("poster_path")
    @Expose
    private String posterPath;
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("backdrop_path")
    @Expose
    private String backdropPath;
    @SerializedName("original_title")
    @Expose
    private String originalTitle;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("vote_average")
    @Expose
    private double voteAverage;
    @SerializedName("overview")
    @Expose
    private String overview;
    @SerializedName("release_date")
    @Expose
    private String releaseDate;

    public Movie(int uniqId, int id, int voteCount, String title, String originalTitle,
                 String overview, String posterPath, String backdropPath, String releaseDate,
                 double voteAverage) {
        this.uniqId = uniqId;
        this.voteCount = voteCount;
        this.posterPath = posterPath;
        this.id = id;
        this.backdropPath = backdropPath;
        this.originalTitle = originalTitle;
        this.title = title;
        this.voteAverage = voteAverage;
        this.overview = overview;
        this.releaseDate = releaseDate;
    }

    @Ignore
    public Movie(int id, int voteCount, String title, String originalTitle, String overview,
                 String posterPath, String backdropPath, String releaseDate, double voteAverage) {
        this.voteCount = voteCount;
        this.posterPath = posterPath;
        this.id = id;
        this.backdropPath = backdropPath;
        this.originalTitle = originalTitle;
        this.title = title;
        this.voteAverage = voteAverage;
        this.overview = overview;
        this.releaseDate = releaseDate;
    }

    public int getUniqId() {
        return uniqId;
    }

    public void setUniqId(int uniqId) {
        this.uniqId = uniqId;
    }

    public int getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public String getPosterPath() {

        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getBigPosterPath() {
        return BASE_URL_POSTER + SIZE_BIG_POSTER +posterPath;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public Integer converterToPercent(){
        return (int) getVoteAverage()*100/10;
    }

    public String getSmalPoster(){
        return BASE_URL_POSTER + SIZE_SMALL_POSTER + getPosterPath();
    }

    public String getYear(){
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format =
                new SimpleDateFormat("yyyy-M-dd");
        Date date = null;
        try {
            date = format.parse(getReleaseDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return Integer.toString(calendar.get(Calendar.YEAR));
    }

    public String pathSiteMovie(){
        return BASE_URL_MOVIE_SITE + "movie/" + id;
    }

}
