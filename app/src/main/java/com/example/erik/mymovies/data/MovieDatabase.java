package com.example.erik.mymovies.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.erik.mymovies.pojo.Movie;

@Database(entities = {Movie.class, FavouriteMovies.class}, version = 6, exportSchema = false)
public abstract class MovieDatabase extends RoomDatabase {

    private static MovieDatabase movieDatabase;
    private static final String DB_NAME = "movies.db";
    private static final Object LOCK = new Object();

    public static MovieDatabase getInstance(Context context){
        synchronized (LOCK) {
            if (movieDatabase == null) {
                movieDatabase = Room.databaseBuilder(context, MovieDatabase.class, DB_NAME).
                        fallbackToDestructiveMigration().build();
            }
        }
        return movieDatabase;
    }

    public abstract MovieDAO getMovieDAO();
}
