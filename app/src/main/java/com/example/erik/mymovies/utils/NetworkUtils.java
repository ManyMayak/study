package com.example.erik.mymovies.utils;

import android.net.Uri;


public class NetworkUtils {

    private static final String API_FLAGS = "http://www.geognos.com/api/en/countries/flag/%s.png";

    public static Uri buildFlagsUrl(String code){
        return Uri.parse(String.format(API_FLAGS, code)).
                buildUpon().
                build();
    }

}
