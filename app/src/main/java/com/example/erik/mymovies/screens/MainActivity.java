package com.example.erik.mymovies.screens;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.example.erik.mymovies.R;
import com.example.erik.mymovies.adapters.MovieAdapter;
import com.example.erik.mymovies.pojo.Movie;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity{

    private MovieAdapter mAd;
    private Switch switchSort;
    private TextView tvPopular, tvTopRated;
    private ProgressBar pbFilms;

    private View.OnClickListener onClickListener;

    private MainViewModel viewModel;

    private static int methodSort;

    private static String lang;

    public static final int POPULARITY = 0;
    public static final int AVERAGE_VOTE = 1;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.item_main:
                Intent intentMain = new Intent(this, MainActivity.class);
                startActivity(intentMain);
                break;
            case R.id.item_favourite:
                Intent intentFavourite = new Intent(this, FavouriteActivity.class);
                startActivity(intentFavourite);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lang = Locale.getDefault().
                getLanguage();

        tvPopular = findViewById(R.id.textViewPopular);
        tvTopRated = findViewById(R.id.textViewRated);

        createOnClickListener();

        tvTopRated.setOnClickListener(onClickListener);
        tvPopular.setOnClickListener(onClickListener);

        switchSort = findViewById(R.id.switchSort);
        pbFilms = findViewById(R.id.progressBarLoading);
        RecyclerView rvMovies = findViewById(R.id.recyclerViewMovies);

        createMovieAdapter();

        rvMovies.setLayoutManager(new GridLayoutManager(this, getColumnCount()));
        rvMovies.setAdapter(mAd);

        createMainViewModel();

        switchSort.setChecked(true);

        setChangeListener();

        switchSort.setChecked(false);
        switchSort.setChecked(false);


    }

    private void createOnClickListener(){
        onClickListener = view -> {

            if (view.getId() == R.id.textViewPopular) {
                switchSort.setChecked(false);
            } else {
                switchSort.setChecked(true);
            }
        };
    }

    private void createMovieAdapter(){
        mAd = new MovieAdapter();
        mAd.setArrayListMovie(new ArrayList<>());

        mAd.setOnClickRecyclerItem(position -> {
            Movie movie = mAd.getArrayListMovie().
                    get(position);
            Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
            intent.putExtra("id", movie.getId());
            startActivity(intent);
        });

        mAd.setOnReachEndListener(() -> {

            if(!viewModel.isLoading() && MainViewModel.getPage() != 1){
                pbFilms.setVisibility(View.VISIBLE);

                viewModel.loadData(methodSort, lang);
            }
        });
    }

    private int getColumnCount(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().
                getMetrics(displayMetrics);
        int width = (int)(displayMetrics.widthPixels / displayMetrics.density);
        return width/185 > 2   ? width/185 : 2;
    }

    private void setChangeListener(){
        switchSort.setOnCheckedChangeListener((compoundButton, b) -> {

            MainViewModel.setPage(1);
            setMethodOfSort(b);
        });
    }

    private void setMethodOfSort(boolean isTopRated){
        if (isTopRated){
            methodSort = AVERAGE_VOTE;
            tvTopRated.setTextColor(getResources().getColor(R.color.colorAccent));
            tvPopular.setTextColor(getResources().getColor(R.color.white));
        } else {
            methodSort = POPULARITY;
            tvPopular.setTextColor(getResources().getColor(R.color.colorAccent));
            tvTopRated.setTextColor(getResources().getColor(R.color.white));
        }

        viewModel.loadData(methodSort, lang);
    }

    private void createMainViewModel(){
        viewModel = ViewModelProvider.AndroidViewModelFactory.
                getInstance(getApplication()).
                create(MainViewModel.class);

        LiveData<List<Movie>> moviesFromLiveData = viewModel.getMovies();
        moviesFromLiveData.observe(this, movies -> {
            if (movies!=null && movies.size()>0) {

                if (MainViewModel.getPage()  >= 2) {
                    mAd.clear();
                }
                mAd.addMovie(movies);

            }
            viewModel.setLoading();
            pbFilms.setVisibility(View.INVISIBLE);
        });

    }
}
