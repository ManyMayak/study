package com.example.erik.mymovies.data;

import androidx.room.Entity;
import androidx.room.Ignore;

import com.example.erik.mymovies.pojo.Movie;

@Entity(tableName = "favourite_movies")
public class FavouriteMovies extends Movie {

    public FavouriteMovies(int uniqId, int id, int voteCount, String title, String originalTitle,
                           String overview, String posterPath,  String backdropPath,
                           String releaseDate, double voteAverage) {
        super(uniqId, id, voteCount, title, originalTitle, overview, posterPath,
                backdropPath, releaseDate, voteAverage);
    }

    @Ignore
    public FavouriteMovies(Movie movie){
        super(movie.getUniqId(), movie.getId(), movie.getVoteCount(), movie.getTitle(),
                movie.getOriginalTitle(), movie.getOverview(), movie.getPosterPath(),
                movie.getBackdropPath(), movie.getReleaseDate(), movie.
                        getVoteAverage());
    }
}
