package com.example.erik.mymovies.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.erik.mymovies.R;
import com.example.erik.mymovies.pojo.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private List<Movie> arrayListMovie;
    private OnClickRecyclerItem onClickRecyclerItem;
    private OnReachEndListener onReachEndListener;

    public interface OnClickRecyclerItem{
        void onClick(int position);
    }

    public interface OnReachEndListener{
        void onReachEnd();
    }

    public MovieAdapter() {
        this.arrayListMovie = new ArrayList<>();
    }

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.posterlist_item, parent, false);
        return new MovieViewHolder(view);
    }

    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p>
     * Override {@link #onBindViewHolder(ViewHolder, int, List)} instead if Adapter can
     * handle efficient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        checkReachEnd(position);

        Movie movie = arrayListMovie.get(position);
        Picasso.get().
                load(movie.getSmalPoster()).
                into(holder.imageViewPoster);
    }

    private void checkReachEnd(int position) {
        if (getItemCount()>=20 && position > getItemCount()-4 && onReachEndListener!=null){
            onReachEndListener.onReachEnd();
        }
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return arrayListMovie.size();
    }

    public void clear(){
        this.arrayListMovie.clear();
        notifyDataSetChanged();
    }

    public List<Movie> getArrayListMovie() {
        return arrayListMovie;
    }

    public void addMovie(List<Movie> movies){
        this.arrayListMovie.addAll(movies);
        notifyDataSetChanged();
    }

    public void setArrayListMovie(List<Movie> arrayListMovie) {
        this.arrayListMovie = arrayListMovie;
        notifyDataSetChanged();
    }

    public void setOnClickRecyclerItem(OnClickRecyclerItem onClickRecyclerItem) {
        this.onClickRecyclerItem = onClickRecyclerItem;
    }

    public void setOnReachEndListener(OnReachEndListener onReachEndListener) {
        this.onReachEndListener = onReachEndListener;
    }

    class MovieViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewPoster;

        private MovieViewHolder(@NonNull View itemView) {
            super(itemView);

            imageViewPoster = itemView.findViewById(R.id.imageViewSmallPoster);

            itemView.setOnClickListener(view -> {
                if (onClickRecyclerItem != null){
                    onClickRecyclerItem.onClick(getAdapterPosition());
                }
            });
        }
    }
}
